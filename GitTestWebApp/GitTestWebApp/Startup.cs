﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GitTestWebApp.Startup))]
namespace GitTestWebApp
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
